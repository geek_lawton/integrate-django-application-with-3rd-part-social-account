This project aim at allows 
integrated authentication app that enable user(s) to 
Login, logout, Signup and account management of both local and social accounts.

===================================
Running the mydjango-allauth project
====================================
Read the requirements from the requirements.txt file
Assuming you use windows cmd, follow these steps to download and run the
mydjango-allauth project in this directory:

::
    cd mydjango-allauth
    pip install django-allauth
	
Now we need to create the database tables and an admin user.
Run the following and when prompted to create a superuser choose yes and
follow the instructions:

::

	python manage.py migrate
    python manage.py createsuperuser


Now you need to run the Django development server:

::

     python manage.py runserver

You should then be able to open your browser on http://127.0.0.1:8000 and
see a page with links to sign in or sign up.