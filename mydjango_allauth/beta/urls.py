from django.conf.urls import url
from beta.views import home

urlpatterns = [
    url(r'^', home.as_view(), name='home'),
]
